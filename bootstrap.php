<?php

//Define consts

  //System full root path
  define('ROOT_DIR', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR);

  //Public full path
  define('PUBLIC_DIR', ROOT_DIR.'public'.DIRECTORY_SEPARATOR);

  //Vendor full path
  define('VENDOR_DIR', ROOT_DIR.'vendor'.DIRECTORY_SEPARATOR);
  
