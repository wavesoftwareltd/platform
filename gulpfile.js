var gulp = require('gulp'),
    gutil = require('gulp-util'),
    browserify = require('gulp-browserify'),
    compass = require('gulp-compass'),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps');

var env,
    jsSources,
    sassSources,
    vendorJsSources,
    vendorSassSources,
    jqueryResize,
    outputDir,
    sassStyle;

env = process.env.NODE_ENV || 'development';
outputDir = 'public/assets/';

if (env==='development') {
  sassStyle = 'compressed';
} else {
  sassStyle = 'compressed';
}


jqueryResize = 'resources/js/vendor/jquery.flot.resize.js';

vendorJsSources = [
  'resources/js/vendor/angular.min.js',
  'resources/js/vendor/angular-resource.min.js',
  'resources/js/vendor/jquery-1.9.1.min.js',
  'resources/js/vendor/jquery-ui-1.10.0.custom.min.js',
  'resources/js/vendor/bootstrap.min.js',
  'resources/js/vendor/jquery.flot.js',
  'resources/js/vendor/jquery.flot.pie.js',
  'resources/js/vendor/jquery.flot.resize.js'
];

vendorSassSources = [
    'resources/sass/vendor/bootstrap.min.scss',
    'resources/sass/vendor/bootstrap-responsive.min.scss',
    'resources/sass/vendor/font-awesome.min.scss',
    'resources/sass/vendor/jquery-ui-1.10.0.custom.min.scss',
];

jsSources = [
  'resources/js/Application.js',
  'resources/js/area.js',
  'resources/js/donut.js',
  'resources/js/users.js'
];

sassSources = [
    'resources/sass/base-admin-3.scss',
    'resources/sass/base-admin-3-responsive.scss',
    'resources/sass/dashboard.scss',
    'resources/sass/custom.scss',
];

 
gulp.task('js', function() {
  gulp.src(jsSources)
    .pipe(gulpif(env === 'development',sourcemaps.init()))
    .pipe(concat('main.js'))
    .pipe(browserify())
    .pipe(gulpif(env === 'production', uglify()))
    .pipe(gulp.dest(outputDir + 'js'))
    .pipe(gulpif(env === 'development', sourcemaps.write(outputDir)))
});

gulp.task('compass', function() {
  gulp.src(sassSources)
    .pipe(gulpif(env === 'development',sourcemaps.init(outputDir)))
    .pipe(compass({
      sass: 'resources/sass',
      style: sassStyle
    })
    .pipe(concat('main.css'))
    .on('error', gutil.log))
    .pipe(gulp.dest(outputDir + 'css'))
    .pipe(gulpif(env === 'development', sourcemaps.write(outputDir)))
});

 
gulp.task('js_vendor', function() {
  gulp.src(vendorJsSources)
    .pipe(gulpif(env === 'development',sourcemaps.init()))
    .pipe(concat('base.js'))
    .pipe(gulpif(env === 'production', uglify()))
    .pipe(gulp.dest(outputDir + 'js'))
    .pipe(gulpif(env === 'development', sourcemaps.write(outputDir)))
});

gulp.task('compass_vendor', function() {
  gulp.src(vendorSassSources)
    .pipe(gulpif(env === 'development',sourcemaps.init(outputDir)))
    .pipe(compass({
      sass: 'resources/sass',
      style: sassStyle
    })
    .pipe(concat('base.css'))
    .on('error', gutil.log))
    .pipe(gulp.dest(outputDir + 'css'))
    .pipe(gulpif(env === 'development', sourcemaps.write(outputDir)))
});

gulp.task('watch', function() {
  gulp.watch(jsSources, ['js']);
  gulp.watch('resources/sass/*.scss', ['compass']);
  gulp.watch(vendorJsSources, ['js_vendor']);
  gulp.watch('resources/sass/vendor/*.scss', ['compass_vendor']);
});


gulp.task('default', ['js', 'compass','js_vendor', 'compass_vendor', 'watch']);