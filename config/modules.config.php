<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

/**
 * List of enabled modules for this application.
 *
 * This should be an array of module namespaces used in the application.
 */
return [
    'Zend\Mail',
    'Zend\Mvc\Console',
    'Zend\Session',
    'Zend\ServiceManager\Di',
    'Zend\Db',
    'Zend\Log',
    'Zend\Cache',
    'Zend\Form',
    'Zend\InputFilter',
    'Zend\Filter',
    'Zend\Paginator',
    'Zend\Hydrator',
    'Zend\Router',
    'Zend\Validator',
    'DoctrineModule',
    'DoctrineORMModule',    
    'ZF\ApiProblem',
    'ZF\ContentNegotiation',
    'ZF\OAuth2',
    'Arbel',
    'Arbel\Admin',
    'ZF\Versioning',
    'ZF\Rpc',
    'ZF\MvcAuth',
    'ZF\Hal',
    'ZF\Rest',
    'ZF\ContentValidation',
    'ZF\Apigility',
    'Application',
];
