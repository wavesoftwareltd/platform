<?php
namespace Application\Controller\Factory\Api;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\Api\UserController;

/**
 * This is the factory for UserController. Its purpose is to instantiate the
 * controller.
 */
class UserControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, 
                     $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        
        // Instantiate the controller and inject dependencies
        return new UserController($entityManager);
    }
}