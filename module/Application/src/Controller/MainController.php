<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Arbel\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Arbel\Sms\SmsInterface;

/**
 * Main controller 
 */
class MainController extends AbstractActionController
{

    public function setInjections(SmsInterface $sms)
    {
        $sms->addTo('972545742747')
            ->setFrom('972526264227')
            ->setMessage('הודעה כלשהי')
            ->send();
    }

    public function indexAction()
    {
//        $this->getModel('Arbel\Model\Email')
//            ->loadByKey('code', 'reset_password')
//            ->setParameters(array(
//                'name' => 'full name',
//                'link' => 'http://somelink.com',
//                )
//            )
//            ->send();

      
        exit('***exit***');
        return new ViewModel();
    }
}