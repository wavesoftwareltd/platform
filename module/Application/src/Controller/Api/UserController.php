<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller\Api;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Doctrine\ORM\EntityManager;
use Application\Entity\User;
use Application\Form\User as UserForm;
use Application\Filter\User as UserFilter;

/**
 * User REST Api controller
 */
class UserController extends AbstractRestfulController {

    /**
     * Entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Constructor method is used to inject dependencies to the controller.
     */
    public function __construct($entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * Get Doctrine entity manager 
     * @var \Doctrine\ORM\EntityManager
     */
    public function getEntityManager() {
        return $this->entityManager;
    }

    /**
     * Create User by data 
     * @param array $data
     */
    public function create($data) {

        $form = new UserForm();
        $userFilter = new UserFilter();
        $result = array();
        $userFilter->setRepository(
                $this->getEntityManager()->getRepository('Application\Entity\User')
        );
        $form->setInputFilter(
                $userFilter->getInputFilter()
        );

        $form->setData($data);

        //If data was valid
        if ($form->isValid()) {
            // Create new User entity.
            $data = $form->getData();
            $user = new User();
            $user->setUsername($data['username']);
            $user->setEmail($data['email']);
            $user->setFullName($data['full_name']);
            // Add the entity to entity manager.
            $this->getEntityManager()->persist($user);

            // Apply changes to database.
            $this->getEntityManager()->flush();
            $result = [
                'user_id' => $user->getId(),
                'username' => $user->getUsername(),
                'email' => $user->getEmail(),
                'full_name' => $user->getFullName()
            ];
        } else {
            $this->getResponse()->setStatusCode(400);
            $result = $form->getMessages();
        }

        return new JsonModel($result);
    }

    /**
     * Get List of all users
     * @return JsonModel
     */
    public function getList() {
        $users = $this->getEntityManager()->getRepository(User::class)->findAll();
        $result = array();
        foreach ($users as $user) {
            $result[] = array(
                'user_id' => $user->getId(),
                'username' => $user->getUsername(),
                'email' => $user->getEmail(),
                'full_name' => $user->getFullName()
            );
        }
        return new JsonModel($result);
    }

    /**
     * Get user by id
     * @param int $id
     * @return JsonModel
     */
    public function get($id) {
        $result = [];
        // report error, redirect, etc.
        $user = $this->getEntityManager()->getRepository(User::class)->find($id);
        if ($user->getId()) {
            $result = [
                'user_id' => $user->getId(),
                'username' => $user->getUsername(),
                'email' => $user->getEmail(),
                'full_name' => $user->getFullName()
            ];
        }

        return new JsonModel($result);
    }

}
