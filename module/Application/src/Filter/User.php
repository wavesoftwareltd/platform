<?php

namespace Application\Filter;

// Add these import statements
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use DoctrineModule\Validator\NoObjectExists;

/**
 * User filter for form input
 */
class User implements InputFilterAwareInterface {

    /**
     * Input filter
     * @var InputFilterInterface 
     */
    protected $inputFilter;

    /**
     * Doctrine repository
     * @var \Doctrine\ORM\EntityRepository 
     */
    protected $repository;

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Set Doctrine repository 
     * @param \Doctrine\ORM\EntityRepository $repository
     * @return $this
     */
    public function setRepository(\Doctrine\ORM\EntityRepository $repository) {
        $this->repository = $repository;
        return $this;
    }

    /**
     * Get input filter
     * @return InputFilter
     * @throws \Exception - when no Doctrine repository was set 
     */
    public function getInputFilter() {
        if (!$this->inputFilter) {
            if (!$this->repository) {
                throw new \Exception("Need repository for validation");
            }
            $inputFilter = new InputFilter();

            //Validate if the email not exist allreay 
            $this->appendExistenceEmailValidator($inputFilter);
            
            //Validate if the username not exist allready
            $this->appendExistenceUsernameValidator($inputFilter);

            //Add full name field
            $inputFilter->add(array(
                'name' => 'full_name',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 255,
                        ),
                    ),
                ),
            ));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * Appends doctrine's noobjectexists validator to validator chain only when required.
     * Check if user exist with specific email
     * 
     * @param InputFilterInterface $inputFilter
     */
    protected function appendExistenceEmailValidator(InputFilterInterface &$inputFilter) {
        $validatorSignature = array(
            'name' => 'email',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ),
                ),
                array(
                    'name' => 'EmailAddress'
                ),
                array(
                    'name' => 'DoctrineModule\Validator\NoObjectExists',
                    'options' => array(
                        'object_repository' => $this->repository,
                        'fields' => 'email',
                        'messages' => array(NoObjectExists::ERROR_OBJECT_FOUND => "User with this email already exists.")
                    )
                ),
            )
        );
        $inputFilter->add($validatorSignature);
    }

    /**
     * Appends doctrine's noobjectexists validator to validator chain only when required.
     * Check if user exist with specific username
     *  
     * @param InputFilterInterface $inputFilter
     */
    protected function appendExistenceUsernameValidator(InputFilterInterface &$inputFilter) {
        $validatorSignature = array(
            'name' => 'username',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ),
                ),
                array(
                    'name' => 'DoctrineModule\Validator\NoObjectExists',
                    'options' => array(
                        'object_repository' => $this->repository,
                        'fields' => 'username',
                        'messages' => array(NoObjectExists::ERROR_OBJECT_FOUND => "User with this username already exists.")
                    )
                ),
            )
        );
        $inputFilter->add($validatorSignature);
    }

}
