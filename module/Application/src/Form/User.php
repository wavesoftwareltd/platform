<?php

namespace Application\Form;

 use Zend\Form\Form;

 /**
  * User form class
  */
 class User extends Form
 {
     /**
      * User form name
      * 
      * @var string
      */
     protected $name = 'user_form';
     
     /**
      * Add the user fields 
      */
     public function __construct()
     {
         // we want to ignore the name passed
         parent::__construct($this->name);

         $this->add(array(
             'name' => 'username',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Username',
             ),
         ));
         $this->add(array(
             'name' => 'email',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Email',
             ),
         ));
         $this->add(array(
             'name' => 'full_name',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Full Name',
             ),
         ));
         
         $this->add(array(
             'name' => 'submit',
             'type' => 'Submit',
             'attributes' => array(
                 'value' => 'Go',
                 'id' => 'submitbutton',
             ),
         ));
     }
 }