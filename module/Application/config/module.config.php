<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Arbel\Service\Factory\DiServiceFactory;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/core[/:action]',
                    'defaults' => [
                        'controller' => Controller\MainController::class,
                        'action' => 'index',
                        'is_use_acl' => true,
                    ],
                ],
            ],
//            'user_api' => [
//                'type' => Segment::class,
//                'options' => [
//                    'route' => '/api/user',
//                    'defaults' => [
//                        'controller' => Controller\Api\UserController::class,
//                        'is_use_acl' => true,
//                    ],
//                ],
//            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\MainController::class => DiServiceFactory::class,
            Controller\Api\UserController::class => Controller\Factory\Api\UserControllerFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'application/main/index' => __DIR__ . '/../view/application/main/index.phtml',
            'application/main/index/quick_stats' => __DIR__ . '/../view/application/main/index/quick_stats.phtml',
            'application/main/index/users' => __DIR__ . '/../view/application/main/index/users.phtml',
            'application/main/index/recent' => __DIR__ . '/../view/application/main/index/recent.phtml',
            'application/main/index/content' => __DIR__ . '/../view/application/main/index/content.phtml',
            'application/main/index/quick_shortcuts' => __DIR__ . '/../view/application/main/index/quick_shortcuts.phtml',
            'application/main/index/chart' => __DIR__ . '/../view/application/main/index/chart.phtml',
            'application/main/index/table' => __DIR__ . '/../view/application/main/index/table.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ],
    // Doctrine config
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        )
    ),
    //Updater current version
    'updater' => '0.0.2'
    
];
